﻿using System;

namespace TurboAwesomeProject.NoughtsAndCrosses

{

    /*
     * so I want to play Naughts and Crosses 
        https://noughts-and-crosses.com/

        right now, let's talk about the game
        We have 2 players
        player1 and player2
        we can reference them as integers - there is no need for anything complicated here
        int player1 = 1;
        int player2 = 2;

        Create a class called Game
        Within this class we need a board
        int[,] board;

        In this class we will need a method - I suggest play
        this method must accept the following:
        1. The player that is playing their move
        2. The row and column they selected

        and the board must update, and detect when a winner has won
     */
    public class Game
    {
        public int[,] board { get; set; }
        public int countPlayer1 { get; set; }
        public int countPlayer2 { get; set; }
        public bool gameStarted = true;
        public Game()
        {
            board = new int[3, 3]{ 
                {0, 0, 0 },
                {0, 0, 0 },
                {0, 0, 0 }
                };
            countPlayer1 = 1;
            countPlayer2 = 2;
        }

        public bool Play(int rowIndex, int columnIndex, int player)
        {
            if (player == 1 || player == 2)
            {
                if(board[rowIndex, columnIndex] == 0)
                {
                    board[rowIndex, columnIndex] += player;
                    return true;
                }else
                {
                    Console.WriteLine("Cannot do that");
                    return false;
                }
                if (//Compare Rows
                    (board[0, 0]).Equals(player) && (board[0, 1]).Equals(player) && (board[0, 2]).Equals(player) ||
                    (board[1, 0]).Equals(player) && (board[1, 1]).Equals(player) && (board[1, 2]).Equals(player) ||
                    (board[2, 0]).Equals(player) && (board[2, 1]).Equals(player) && (board[2, 2]).Equals(player) ||
                    //Compare cross-diagonal
                    (board[0, 0]).Equals(player) && (board[1, 1]).Equals(player) && (board[2, 2]).Equals(player) ||
                    (board[0, 2]).Equals(player) && (board[1, 1]).Equals(player) && (board[2, 0]).Equals(player) ||
                    //Compare Columns
                    (board[0, 0]).Equals(player) && (board[1, 0]).Equals(player) && (board[2, 0]).Equals(player) ||
                    (board[0, 1]).Equals(player) && (board[1, 1]).Equals(player) && (board[2, 1]).Equals(player) ||
                    (board[0, 2]).Equals(player) && (board[1, 2]).Equals(player) && (board[2, 2]).Equals(player))
                {
                    Console.WriteLine($"Player { player} wins");
                    return gameStarted = false; 
                }
                else if (//Check for draw result
                  board[0, 0] != 0 && board[0, 1] != 0 && board[0, 2] != 0 &&
                  board[1, 0] != 0 && board[1, 1] != 0 && board[1, 2] != 0 &&
                  board[2, 0] != 0 && board[2, 1] != 0 && board[2, 2] != 0)
                {
                    Console.WriteLine("It is a draw");
                    return gameStarted = false;
                }
            }
            return gameStarted = true;
        }
    }
}
