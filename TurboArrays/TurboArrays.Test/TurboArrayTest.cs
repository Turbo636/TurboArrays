using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.IO;
using TurboAwesomeProject.TurboArrays;

namespace TurboAwesomeProject.TurboArrays.Test
{
    [TestClass]
    public class TurboArrayTest
    {

        //Testing if test return a true value when incrementing an index
        [TestMethod]
        public void turboAddToArray_Test()
        {
            //Arrange
            TurboArray turbo = new TurboArray();
            //Act
            var result = turbo.turboAddToArray(1);
            //Assert
            //The return result is successful
            Assert.AreEqual(true, result);
        }

        //Incrementing Index 0 by 1.
        [TestMethod]
        public void turboAddToArray_ArrayIncreaseWithIndexOne_Test()
        {
            TurboArray turbo = new TurboArray();
            int i = turbo.turbo[0];
            var result = turbo.turboAddToArray(1);
            Assert.AreEqual(i + 1, turbo.turbo[0]);
        }

        //Incrementing Index 9 by 1
        [TestMethod]
        public void turboAddToArray_ArrayIncreaseWithIndexNine_Test()
        {
            TurboArray turbo = new TurboArray();
            int i = turbo.turbo[9];
            var result = turbo.turboAddToArray(10);
            Assert.AreEqual(i + 1, turbo.turbo[9]);
        }

        //Returning false with invalid negative values
        [TestMethod]
        public void turboArray_InvalidValues_NegativeValues_Test()
        {
            //Arrange
            TurboArray turbo = new TurboArray();
            //Act
            var result = turbo.turboAddToArray(-1);
            //Assert
            Assert.IsFalse(result);
        }

        //Returning false with zero as the index when adding to Array
        [TestMethod]
        public void turboArray_InvalidValues_ZeroValue_Test()
        {
            //Arrange
            TurboArray turbo = new TurboArray();
            //Act
            var result = turbo.turboAddToArray(0);
            //Assert
            Assert.IsFalse(result);
        }

        //Returning false with values than the expected size of Array
        [TestMethod]
        public void turboArray_InvalidValues_HigherIndexValues_Test()
        {
            //Arrange
            TurboArray turbo = new TurboArray();
            //Act
            var result = turbo.turboAddToArray(20);
            //Assert
            Assert.IsFalse(result);
        }
    }
}
