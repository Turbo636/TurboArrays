using Microsoft.VisualStudio.TestTools.UnitTesting;
using TurboAwesomeProject.NoughtsAndCrosses;

namespace TurboAwesomeProject.NoughtsAndCrosses_Test
{
    [TestClass]
    public class game_Test
    {
        [TestMethod]
        public void gamePlay_Player1_Test()
        {
            //Arrange
            Game board = new Game();
            //Act
            var result = board.Play(0, 0, 1);
            //Assert
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void gamePlay_Player2_Test()
        {
            //Arrange
            Game board = new Game();
            //Act
            var result = board.Play(1, 0, 2);
            //Assert
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void gamePlay_Winner_Test()
        {
            //Arrange
            Game board = new Game();
            var expected = new int[3, 3] { { 1, 1, 1 }, { 2, 0, 0 }, { 0, 2, 0 } };
            //Act
            board.Play(0, 0, 1);
            board.Play(1, 0, 2);
            board.Play(0, 1, 1);
            board.Play(2, 1, 2);
            board.Play(0, 2, 1);
            //Assert
            for (int i = 0; i < board.board.GetLength(0); i++)
            {
                for (int j = 0; j < expected.GetLength(1); j++)
                {
                    Assert.AreEqual(expected[i, j], board.board[i, j]);
                }
            }
            //We have a winner when gameStarted is false
            Assert.IsFalse(board.gameStarted);
        }

        [TestMethod]
        public void gamePlay_Draw_Test()
        {
            //Arrange
            Game board = new Game();
            var expected = new int[3, 3] { { 1, 1, 2 }, { 2, 2, 1 }, { 1, 1, 2 } };
            //Act
            board.Play(0, 1, 1);
            board.Play(0, 2, 2);
            board.Play(1, 2, 1);
            board.Play(1, 1, 2);
            board.Play(2, 1, 1);
            board.Play(2, 2, 2);
            board.Play(2, 0, 1);
            board.Play(1, 0, 2);
            board.Play(0, 0, 1);
            //Assert
            for (int i = 0; i < board.board.GetLength(0); i++)
            {
                for (int j = 0; j < expected.GetLength(1); j++)
                {
                    Assert.AreEqual(expected[i, j], board.board[i, j]);
                }
            }
        }
    }
}
