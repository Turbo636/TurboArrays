﻿using System;

namespace TurboAwesomeProject.TurboArrays
{
    /*
     * My requirements are:
        I want a C# class that can do this:

        1. I want a 1-d array of 10 numbers 
        2. When I input a number between 1-10 - I want it to increment the number in that box
            Refactor your logic to do this:
            (I think this might be easier to understand)

            [0,0,0,0,0,0,0,0,0]
            If I input the number 3
            [0,0,1,0,0,0,0,0,0]
            Then I input 1
            [1,0,1,0,0,0,0,0,0]
            Then I input 3 again
            [1,0,2,0,0,0,0,0,0]

            If I input <= 0 or >= 10
            The array stays the same
        3. I also expect your unit tests to cover cases if I provide it something that is not-numeric, 
            or if the number is less than 1 or if the number is greater than 10
        3.1 I expect you to handle the error gracefully (no exceptions)

        Feel free to write a console application to demonstrate your work, 
            but I want your unit tests to guide your component

     */

    public class TurboArray
    {
        //Having get; set; here is not normally a problem :)
        public int[] turbo;  // get; set; }
        public bool indx = true;
        public int index { get; set; }
        public TurboArray()
        {
            turbo = new int[10]; //{ 0, 0, 0, 0, 0, 0, 0, 0, 0 };

        }

        public bool turboAddToArray(int index)
        {
            index -= 1;
            if (index < 0 || index > 10)
            {
                return false;
            }
            if (index > -1 && index < 10)
            {
                turbo[index] += 1;
                return true;
            }
            return false;
        }
    }
}
