﻿using System;

/*
             * It looks better, but now i want you to experiment with ways to optomise your code in this method.

                In this same project (no need to create another project)

                Create a new class which also has an array of 10 integers

                I want a method that also accepts an array of 10 integers
                And you must add the indexes together

                [0,1,0,0,0,0,0,0,1,0]
                +
                [0,0,0,0,0,0,0,0,1,0]
                =
                [0,1,0,0,0,0,0,0,2,0]
                The arrays MUST be of length 10, do not allow any addition with less than or greater than 10
                Show me all of your unit tests.
                Remember you can add negative numbers too
                So new class and a new unit test class


            I want you to still have only 1 array 
            public class IncreaseArray 
                { 
                    public int[] myTurboArray { get; set; } 
            }
            but I want you addArrayIndex method to accept an array as a parameter
            addArray(int[] arrayIAmAddingWith)
            arrayIAmAddingWith must be the same length as myTurboArray
            and you must add arrayIAmAddingWith[i] to myTurboArray[i]
            an integer in the array can be negative

            If you remember maths
            (2) + (-1)
            =(2) - 1
            = 2 - 1
            = 1
            I want a method that also accepts an array of 10 integers 
            And you must add the indexes together 
 
                            [0,1,0,0,0,0,0,0,1,0] 
                            + 
                            [0,0,0,0,0,0,0,0,1,0] 
                            = 
                            [0,1,0,0,0,0,0,0,2,0]
            remember the specifications
             */

namespace TurboAwesomeProject.IncreaseArrayIndex
{
    public class IncreaseArray
    {
        public int[] myTurboArray { get; set; }
        public IncreaseArray()
        {
            myTurboArray = new int[10] { 4, 0, 0, 0, 1, -2, 0, 0, 0, 0};
        }

        public int[] addArray(int[] arrayIAmAddingWith)
        {
            int[] resultArray = new int[10];
            if (arrayIAmAddingWith.Length == myTurboArray.Length)
            {
                for (int i = 0; i < myTurboArray.Length; i++)
                {
                    resultArray[i] = myTurboArray[i] += arrayIAmAddingWith[i];
                    //return true;
                }
            }
            return resultArray;
        }
    }
}
