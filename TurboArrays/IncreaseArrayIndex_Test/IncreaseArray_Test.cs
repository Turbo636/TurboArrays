using TurboAwesomeProject.IncreaseArrayIndex;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TurboAwesomeProject.IncreaseArrayIndex_Test
{
    [TestClass]
    public class IncreaseArray_Test
    {

        [TestMethod]
        public void addingArrayToExistingArray_Test()
        {
            //Arrange
            IncreaseArray myTurboArray = new IncreaseArray();
            int[] arrayIAmAddingWith = new int[10];
            int[] resultArray = new int[10] { 3, 0, 0, 0, 3, -7, 0, 0, 0, 2 };
            //Act
            arrayIAmAddingWith[0] = -1;
            arrayIAmAddingWith[4] = 2;
            arrayIAmAddingWith[5] = -5;
            arrayIAmAddingWith[9] = 2;
            myTurboArray.addArray(arrayIAmAddingWith);
            //Assert
            Assert.AreEqual(resultArray.ToString(), myTurboArray.addArray(arrayIAmAddingWith).ToString());
        }
    }
}
