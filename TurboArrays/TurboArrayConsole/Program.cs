﻿using System;
using TurboAwesomeProject.TurboArrays;
using TurboAwesomeProject.IncreaseArrayIndex;
using TurboAwesomeProject.NoughtsAndCrosses;

namespace TurboAwesomeProject.TurboArrayConsole
{
    class Program
    {
        public static Game game1 = new Game();
        public static int player = 1;
        static void Main(string[] args)
        {
            var col = 0;
            var row = 0;
            while (game1.gameStarted)
            {
                Console.WriteLine("Let's play a game of Naughts and Crosses");
                Console.WriteLine($"Player {player} turn");
                DisplayBoard();
                Int32.TryParse(Console.ReadLine(), out row);
                Int32.TryParse(Console.ReadLine(), out col);
                if(game1.Play(row, col, player) == true)
                {
                    ChangePlayer();
                }
                    
            }
        }

        public static void DisplayBoard()
        {
            int rowLength = game1.board.GetLength(0);
            int colLength = game1.board.GetLength(1);
            for (int i = 0; i < rowLength; i++)
            {
                for (int j = 0; j < colLength; j++)
                {
                    Console.Write(" ");
                    Console.Write(game1.board[i, j]);
                }
                Console.Write(Environment.NewLine + Environment.NewLine);
            }
        }

        public static void ChangePlayer()
        {
            if (player == 1)
            {
                player = 2;
            }else
            {
                player = 1;
            }
        }
    }
}
