pipeline { 
    agent any

    environment {
        NOW = new Date().format("yyyyMMddHHmm", TimeZone.getTimeZone('UTC'))
        PRIMARYBRANCH = "master"
    }
    triggers {
        gitlab(triggerOnPush: true, triggerOnMergeRequest: true, branchFilterType: 'All')
    }
    stages {
        stage ('Clean workspace') 
        {
            steps {
                cleanWs()
            }
        }
        stage ('Checkout') 
        {
            steps {
                checkout(
                    [
                        $class: 'GitSCM', 
                        branches: [[name: '**']], 
                        browser: [$class: 'GitLabBrowser'], 
                        extensions: [], 
                        userRemoteConfigs: [
                            [
                                credentialsId: 'GitLab_grebisz_UserPass', 
                                url: 'https://gitlab.com/Turbo636/TurboArrays'
                            ]
                        ]]
                )
                
           }
       }
        stage ('Build')
        {
            steps {
                sh "dotnet restore ./TurboArrays/TurboArrays.sln"
                sh "dotnet build ./TurboArrays/TurboArrays.sln"
                sh "dotnet test ./TurboArrays/noughts-and-crosses_Test/noughts-and-crosses_Test.csproj --logger:trx /p:CollectCoverage=true /p:CoverletOutputFormat=opencover"
            }
        }
        try{
            stage ('Test')
            {
                steps{
                    withSonarQubeEnv("RebiszIndustries_data") {
                        script{
                            try{
                                sh "dotnet tool install --global dotnet-sonarscanner --version 5.2.2"
                            }
                            catch(Exception ex){
                            
                            }    
                        }
                        sh "/root/.dotnet/tools/dotnet-sonarscanner begin /k:'turbo_noughts-and-crosses' /d:sonar.login='42a789bed3a0c75af80e54f9f94f1b1bbd2d80b6' -d:sonar.host.url='http://data.rebiszindustries.com:9000/' -d:sonar.cs.opencover.reportsPaths='./TurboArrays/noughts-and-crosses_Test/coverage.opencover.xml'"
                        sh "dotnet build"
                        sh "/root/.dotnet/tools/dotnet-sonarscanner end /d:sonar.login='42a789bed3a0c75af80e54f9f94f1b1bbd2d80b6'"
                    }
                    mstest testResultsFile:"**/**/*.trx", keepLongStdio: true
                }
                
            }
        } 
        catch(e)
        {
            
        }

        stage("Quality gate") {
            steps {
                waitForQualityGate abortPipeline: true
            }
        }
    }
    post {
        failure {
            updateGitlabCommitStatus name: 'build', state: 'failed'
        }
        success {
            updateGitlabCommitStatus name: 'build', state: 'success'
        }
    }
}